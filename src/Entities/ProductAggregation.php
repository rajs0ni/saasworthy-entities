<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Product;
use Saasworthy\Entities\SwModel;

class ProductAggregation extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_product_aggregation';
    protected $primaryKey = 'pkproductAggregationId';

    public function product()
    {
        return $this->belongsTo(Product::modelClass(),'fkProductId',);
    }
}
