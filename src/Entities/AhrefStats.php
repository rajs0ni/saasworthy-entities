<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class AhrefStats extends SwModel
{
    const DATA_LIMIT = 10;

    public $timestamps = false;
    protected $table = 'sw_stats_ahrefs';
    protected $connection = DB_CONNECTION_CRAWL;
}