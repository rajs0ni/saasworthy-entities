<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\AwardBuilderLog;


class Seo extends SwModel
{
    const DATA_LIMIT = 10;

    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updated';

    protected $table = 'saas_seo';
    protected $primaryKey = 'pkSeoId';
    
    protected $fillable = [
        'pageType',
        'fkCategoryId',
        'fkProductId',
        'fkblogId',
        'fkEditorPickId',
        'seoSource',
        'indexable',
        'anchorText',
        'metaPageTitle',
        'metaPageDescription',
        'metaKeywords',
        'updateBy',
        'status',
        'gaPageViews7',
        'gaPageViews30',
        'gaPageViewsAllTime',
        'fkDownloadsId',
        'dynamicCategoryDescription',
        'dynamicCategoryName',
        'canonical_url',
        'is_redirected',
        'redirect_url',
        'redirect_reason',
        'noindex_reason',
        'is_follow',
        'amp_url',
        'fkAwardBuilderId',
        'childPage',
        'parentPageId',
        'manual_generation',
        'p1Id',
        'p2Id',
        'p3Id',
        'p4Id',
        'featureCategoryCount',
        'pageAdditionalSummary2',
        'pkfeatureid',
        'pageAdditionalSummary'
    ];

    public function awardBuilder()
    {
        return $this->setConnection(DB_CONNECTION_AWARDS)->belongsTo(AwardBuilderLog::class,'fkAwardBuilderId');
    }
}
