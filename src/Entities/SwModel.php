<?php

namespace Saasworthy\Entities;

use Illuminate\Database\Eloquent\Model;

class SwModel extends Model
{
    public static function modelClass()
    {   
        return get_called_class();
    }

    public static function model()
    {   
        $modelClass = self::modelClass();
        return new $modelClass();
    }
    
    public static function primaryKey()
    {   
        return self::model()->getKeyName();
    }
}
