<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\UseThisActivity;
use Saasworthy\Entities\ShortlistedProduct;

class User extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_users';
    protected $primaryKey = 'pkUserUid';  

    public function shortlistedProducts()
    {
        return $this->hasMany(ShortlistedProduct::modelClass(), 'userId');
    }

    public function productsUsedBy()
    {
        return $this->hasMany(UseThisActivity::modelClass(), 'userId');
    }
}
