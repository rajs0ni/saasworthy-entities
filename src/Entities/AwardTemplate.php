<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\AwardedProduct;

class AwardTemplate extends SwModel
{
    const DATA_LIMIT = 10;
    
    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'createdDate';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updatedDate';

    protected $connection = DB_CONNECTION_AWARDS;

    public function products()
    {
        return $this->hasMany(AwardedProduct::modelClass(),'awardTemplateId');
    }

    public function awardMetric()
    {
        return $this->belongsTo(AwardMetric::class,'metricId');
    }
}