<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class FaqTemplate extends SwModel
{
    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'createdDate';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updatedDate';

    protected $table = 'saas_faq_templates';
}