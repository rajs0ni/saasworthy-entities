<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\Category;

class CommonNotationData extends SwModel
{
   /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated';

    protected $table = 'saas_cnd';
    protected $primaryKey = 'pkcndId';

    public function categories()
    {
        return $this->hasMany(Category::modelClass(),'fkparentCategory');
    }
}