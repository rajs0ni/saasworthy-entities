<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class AwardMetric extends SwModel
{
    const DATA_LIMIT = 10;

    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'createdDate';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updatedDate';

    protected $table = 'award_metrics';
    protected $connection = DB_CONNECTION_AWARDS;
}