<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class Filter extends SwModel
{
    public $timestamps = false;
    
    protected $table = 'saas_filters';
}