<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class ProductMetricScore extends SwModel
{
    const DATA_LIMIT = 10;

    public $timestamps = false;
    protected $table = 'product_metric_scores';
    protected $connection = DB_CONNECTION_AWARDS;

    protected $fillable = [
        'awardTemplateId',
        'productId',
        'awardMetricId',
        'awardMetricValue',
        'awardBuilderId',
        'active',
        'metricDetails'
    ];
}