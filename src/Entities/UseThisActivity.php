<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class UseThisActivity extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_use_this_activity';
}
