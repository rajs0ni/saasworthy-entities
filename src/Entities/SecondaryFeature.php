<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class SecondaryFeature extends SwModel
{
    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updated';

    protected $table = 'saas_secondary_feature';
    protected $primaryKey = 'pkSecFeatureId';
}