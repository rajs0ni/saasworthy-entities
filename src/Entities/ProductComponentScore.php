<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Product;
use Saasworthy\Entities\SwModel;

class ProductComponentScore extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'product_component_score';
    protected $connection = DB_CONNECTION_SCORE;
}
