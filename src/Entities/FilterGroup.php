<?php

namespace Saasworthy\Entities;
use Saasworthy\Entities\Filter;
use Saasworthy\Entities\SwModel;

class FilterGroup extends SwModel
{
    public $timestamps = false;
    
    protected $table = 'saas_filtergroup';

    public function filters()
    {
        return $this->hasMany(Filter::modelClass(),'parent_id');
    }
}