<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class UrlRule extends SwModel
{
    protected $table = 'saas_url_rule';
    protected $primaryKey = 'pkRuleId';
}