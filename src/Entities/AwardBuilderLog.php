<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Seo;
use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\AwardTemplate;

class AwardBuilderLog extends SwModel
{
    const DATA_LIMIT = 10;

    public $timestamps = false;
    protected $table = 'award_builder_logs';
    protected $connection = DB_CONNECTION_AWARDS;

    protected $casts = [
        'awardDetails' => 'object'
    ];
    
    public function seo()
    {
        return $this->setConnection(DB_CONNECTION_DEFAULT)->hasOne(Seo::class,'fkAwardBuilderId');
    }

    public function awardTemplate()
    {
        return $this->belongsTo(AwardTemplate::class,'awardTemplateId');
    }
}