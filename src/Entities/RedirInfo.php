<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class RedirInfo extends SwModel
{
    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'createdAt';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = null;

    protected $table = 'saas_product_redir_info';

    protected $fillable = [
        "product_id",
        "category_id",
        "seo_id",
        "campaign_id",
        "page_name",
        "page_url",
        "page_type",
        "device",
        "country",
        "redirection_type",
        "utm_source",
        "utm_medium",
        "utm_campaign",
        "eventType",
        "eventAction",
        "eventCategory",
        "eventLabel",
        "value",
        "isCustomPage"
    ];
}