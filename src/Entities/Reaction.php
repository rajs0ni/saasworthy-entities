<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class Reaction extends SwModel
{
    protected $table = 'saas_product_reactions';

    public function entity()
    {
        return $this->morphTo();
    }
}