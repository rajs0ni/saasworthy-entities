<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Seo;
use Saasworthy\Entities\Feature;
use Saasworthy\Entities\SwModel;

class Category extends SwModel
{
    const DATA_LIMIT = 6;

    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = 'updated';

    protected $table = 'saas_category';
    protected $primaryKey = 'pkCategoryId';
    
    public function seos()
    {
        return $this->hasMany(Seo::modelClass(),'fkCategoryId');
    }

    public function features(){
        return $this->belongsToMany(Feature::modelClass(),'saas_category_feature', 'fkCategoryId', 'fkfeatureId')
                    ->withPivot(
                        'mappingStatus',
                        'mappingType',
                        'updatedBy'
                    )
                    ->withTimestamps();
    } 

    public function scopePopular($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->orderBy('gaPageViews30', $order);
        })->orderBy('productCount', $order);
    }

    public function scopeTrending($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->orderBy('gaPageViews7', $order);
        })->orderBy('productCount', $order);
    }

    public function scopeLatest($query, $order='DESC')
    {
        return $query->orderBy('created', $order);
    }
}
