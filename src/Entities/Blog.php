<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Seo;
use Saasworthy\Entities\SwModel;

class Blog extends SwModel
{
    const DATA_LIMIT = 10;

    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updated';
    
    protected $table = 'saas_blog';
    protected $primaryKey = 'pkBlogId';

    public function seos()
    {
        return $this->hasMany(Seo::modelClass(),'fkblogId',);
    }

    public function scopeFeatured($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->where('is_redirected', '0')
                ->orderBy('gaPageViews7', $order);
        })->orderBy('publishDate',$order);
    }

    public function scopeLatest($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) {
            $query->where('is_redirected', '0');
        })->orderBy('created', $order);
    }
}
