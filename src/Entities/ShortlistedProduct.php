<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class ShortlistedProduct extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_shortlisted_products';
}
