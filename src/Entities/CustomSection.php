<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class CustomSection extends SwModel
{
    public $timestamps = false;
    
    protected $table = 'saas_products_customSections';
    protected $primaryKey = 'pkcustomSectionId';
}