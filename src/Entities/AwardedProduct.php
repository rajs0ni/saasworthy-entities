<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class AwardedProduct extends SwModel
{
    const DATA_LIMIT = 10;

    public $timestamps = false;
    protected $table = 'awarded_products';
    protected $connection = DB_CONNECTION_AWARDS;

    protected $fillable = [
        'awardTemplateId',
        'productId',
        'rank',
        'awardDate',
        'awardDisplayName',
        'awardImage',
        'awardMetricId',
        'awardMetricValue',
        'awardBuilderId',
        'awardPriority',
        'seoId',
        'active',
        'published',
        'metricDetails',
        'createdDate',
        'categoryId',
        'discarded',
        'rescaledValue',
        'dateString',
        'publishedDate'
    ];
}