<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Product;
use Saasworthy\Entities\SwModel;

class SwScoreComponent extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'sw_score_components';
    protected $connection = DB_CONNECTION_SCORE;
}