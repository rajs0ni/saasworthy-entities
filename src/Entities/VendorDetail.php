<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\UseThisActivity;
use Saasworthy\Entities\ShortlistedProduct;

class VendorDetail extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'vendor_details';
    protected $connection = DB_CONNECTION_UNIVERSAL;
}
