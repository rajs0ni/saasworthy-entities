<?php

namespace Saasworthy\Entities;
use Saasworthy\Entities\Product;
use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\Category;

class ProductVector extends SwModel 
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_product_vectors';

    protected $fillable = [
        'product_id',
        'category_id',
        'product_name',
        'category_name',
        'product_description',
        'vectors'
    ];

    protected $casts = [
        'vectors' => 'array'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}