<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class ProductMetricPreNormalizationScore extends SwModel
{
    const DATA_LIMIT = 10;

    public $timestamps = false;
    protected $table = 'product_metric_scores_pre_normalisation';
    protected $connection = DB_CONNECTION_AWARDS;

    protected $fillable = [
        'awardTemplateId',
        'productId',
        'awardMetricId',
        'awardBuilderId',
        'awardMetricValue',
        'linkedinCountMetricValue',
        'twitterCountMetricValue',
        'facebookCountMetricValue',
        'instagramCountMetricValue',
        'youtubeCountMetricValue',
        'backlinksCountMetricValue',
        'refDomainCountMetricValue',
        'trafficCountMetricValue',
        'ratingCountMetricValue',
        'ratingVelocityMetricValue',
        'ratingDecayMetricValue',
        'active'
    ];
}