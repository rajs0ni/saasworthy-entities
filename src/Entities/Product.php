<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\Seo;
use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\ProductAggregation;

class Product extends SwModel
{
    const DATA_LIMIT = 10;

    /**
    * The name of the "created at" column.
    *
    * @var string|null
    */
    const CREATED_AT = 'created';

    /**
    * The name of the "updated at" column.
    *
    * @var string|null
    */
    const UPDATED_AT = 'updated';

    protected $table = 'saas_products';
    protected $primaryKey = 'pkProductId';

    public function seos()
    {
        return $this->hasMany(Seo::modelClass(),'fkProductId');
    }

    public function aggregation()
    {
        return $this->hasOne(ProductAggregation::modelClass(),'fkProductId');
    }

    public function scopePopular($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->orderBy('gaPageViews30', $order);
        });
    }

    public function scopeFeatured($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->orderBy('gaPageViews30', $order);
        });
    }

    public function scopeTrending($query, $order='DESC')
    {
        return $query->whereHas('seos', function ($query) use ($order) {
            $query->orderBy('gaPageViews7', $order);
        });
    }

    public function scopeLatest($query, $order='DESC')
    {
        return $query->orderBy('created', $order);
    }

    public function reaction()
    {
        return $this->morphOne(Reaction::class, 'entity');
    }
}
