<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;
use Saasworthy\Entities\Category;

class CategoryAggregation extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_category_aggregation';

    public function category()
    {
        return $this->belongsTo(Category::modelClass(),'fkCategoryId');
    }
}
