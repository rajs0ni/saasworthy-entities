<?php

namespace Saasworthy\Entities;

use Saasworthy\Entities\SwModel;

class BlogAggregation extends SwModel
{
    const DATA_LIMIT = 10;

    protected $table = 'saas_blogs_aggregation';
    protected $primaryKey = 'pkBlogAggId';
}
