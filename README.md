
# SaaSworthy Entities

Laravel package to manage all entities or models of SaaSworthy.

## Installation

For the installation, you only need to add the package to your composer.json file as show below.

```shell
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/rajs0ni/saasworthy-entities"
    }
],
"require": {
    "saasworthy/entities": "dev-master"
}
```

after installation you should see

```shell
Discovered Package: saasworthy/entities
```

and you are ready to go
## Basic Usage

As per your need, you can use any entity as listed below. 

## Supported Entities

Currently, we're providing the support of these entities.

| Entity | Namespace | Description |
| :------------ | :------------- | :------------- | 
| AhrefStats | Saasworthy\Entities\AhrefStats | Manage CRUD operations on `sw_crawl_db.sw_stats_ahrefs` table |
| AwardBuilderLog | Saasworthy\Entities\AwardBuilderLog | Manage CRUD operations on `sw_awards_db.award_builder_logs` table |
| AwardedProduct | Saasworthy\Entities\AwardedProduct | Manage CRUD operations on `sw_awards_db.awarded_products` table |
| AwardedMetric | Saasworthy\Entities\AwardedMetric | Manage CRUD operations on `sw_awards_db.award_metrics` table |
| AwardTemplate | Saasworthy\Entities\AwardTemplate | Manage CRUD operations on `sw_awards_db.award_templates` table |
| Blog | Saasworthy\Entities\Blog | Manage CRUD operations on `saasworthy.saas_blog` table |
| BlogAggregation | Saasworthy\Entities\BlogAggregation  |  Manage CRUD operations on `saasworthy.saas_blogs_aggregation` table  |
| Category | Saasworthy\Entities\Category | Manage CRUD operations on `saasworthy.saas_category` table |
| CategoryAggregation | Saasworthy\Entities\CategoryAggregation | Manage CRUD operations on `saasworthy.saas_category_aggregation` table | 
| CommonNotationData | Saasworthy\Entities\CommonNotationData | Manage CRUD operations on `saasworthy.saas_cnd` table |
| CustomSection | Saasworthy\Entities\CustomSection | Manage CRUD operations on `saasworthy.saas_products_customSections` table |
| FaqTemplate | Saasworthy\Entities\FaqTemplate | Manage CRUD operations on `saasworthy.saas_faq_templates` table |
| Feature | Saasworthy\Entities\Feature | Manage CRUD operations on `saasworthy.saas_feature` table |
| Filter | Saasworthy\Entities\Filter | Manage CRUD operations on `saasworthy.saas_filters` table |
| FilterGroup | Saasworthy\Entities\FilterGroup | Manage CRUD operations on `saasworthy.saas_filtergroup` table |
| PricingPlan | Saasworthy\Entities\PricingPlan | Manage CRUD operations on `saasworthy.saas_productPricing_plan` table |
| Product | Saasworthy\Entities\Product | Manage CRUD operations on `saasworthy.saas_products` table |
| ProductAggregation | Saasworthy\Entities\ProductAggregation | Manage CRUD operations on `saasworthy.saas_product_aggregation` table |
| ProductComponentScore | Saasworthy\Entities\ProductComponentScore | Manage CRUD operations on `sw_score_db.product_component_score` table |
| ProductMetricPreNormalizationScore | Saasworthy\Entities\ProductMetricPreNormalizationScore | Manage CRUD operations on `sw_awards_db.product_metric_scores_pre_normalisation` table |
| ProductMetricScore | Saasworthy\Entities\ProductMetricScore | Manage CRUD operations on `sw_awards_db.product_metric_scores` table |
| ProductVector | Saasworthy\Entities\ProductVector | Manage CRUD operations on `saasworthy.saas_product_vectors` table |
| RedirInfo | Saasworthy\Entities\RedirInfo | Manage CRUD operations on `saasworthy.saas_product_redir_info` table |
| SecondaryFeature | Saasworthy\Entities\SecondaryFeature | Manage CRUD operations on `saasworthy.saas_secondary_feature` table |
| Seo | Saasworthy\Entities\Seo | Manage CRUD operations on `saasworthy.saas_seo` table |
| ShortlistedProducts | Saasworthy\Entities\ShortlistedProducts | Manage CRUD operations on `saasworthy.saas_shortlisted_products` table |
| SwScoreComponent | Saasworthy\Entities\SwScoreComponent | Manage CRUD operations on `sw_score_db.sw_score_components` table |
| TargetingIdea | Saasworthy\Entities\TargetingIdea | Manage CRUD operations on `saasworthy.targeting_ideas` table |
| UrlRule | Saasworthy\Entities\UrlRule | Manage CRUD operations on `saasworthy.saas_url_rule` table |
| User | Saasworthy\Entities\User | Manage CRUD operations on `saasworthy.saas_users` table |
| UseThisActivity | Saasworthy\Entities\UseThisActivity | Manage CRUD operations on `saasworthy.saas_use_this_activity` table |
| VendorDetail | Saasworthy\Entities\VendorDetail | Manage CRUD operations on `saas_universe_db.vendor_details` table in SaaS Universal DB |